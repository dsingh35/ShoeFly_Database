CREATE TABLE EMPLOYEE
(
	ID VARCHAR2(10) NOT NULL,
	NAME VARCHAR2(255),
	DEPARTMENT VARCHAR2(255),
	ADDRESS VARCHAR2(255),
	PHONE_NO NUMBER(10,10),
	PRIMARY KEY (ID)
);
CREATE TABLE management
(
	ID VARCHAR2(10) PRIMARY KEY,
	salary int,
	role VARCHAR2(255),
   	FOREIGN KEY(ID) REFERENCES Employee(ID) ON DELETE CASCADE
);
CREATE TABLE staff
(
	ID VARCHAR2(10) PRIMARY KEY,
    	pay int,
    	work_assigned VARCHAR2(255),
    	comments VARCHAR2(255),
  	FOREIGN KEY(ID) REFERENCES Employee(ID) ON DELETE CASCADE
);
CREATE TABLE Accounts 
(
	year int,
    	month varchar2(10),
    	tax int,
     	revenue int,
      	expense int,
	ID varchar2(10),
 	FOREIGN KEY(ID) REFERENCES EMPLOYEE(ID) ON DELETE CASCADE,
 	PRIMARY KEY (year, month)
);
CREATE TABLE customer
(
	cus_id int,
      	CUS_NAME varchar2(255),
     	CUS_email varchar2(255),
     	address varchar2(255),
	PRIMARY KEY(cus_id) 
);
CREATE TABLE serves
(
	cus_id int,
      	ID  varchar2(10),
      	FOREIGN KEY(cus_id) REFERENCES customer(cus_id) ON DELETE CASCADE,
	FOREIGN KEY(ID) REFERENCES employee(ID) ON DELETE CASCADE,
	PRIMARY KEY(cus_id, ID) 
);
CREATE TABLE bill
(
	Invoice_no int,
      	methods  varchar2(255),
      	amount int,
  	due int,
   	pay_date Date,
    	cus_id int NOT NULL,
      	FOREIGN KEY(cus_id) REFERENCES customer(cus_id) ON DELETE CASCADE,
	PRIMARY KEY(cus_id, Invoice_no) 
);
CREATE TABLE rooms
(
	room_no int ,
      	Room_type  varchar2(255),
      	price int,
      	availability boolean,
     	PRIMARY KEY(room_no)
);
CREATE TABLE allots
(
	room_no int ,
     	ID varchar2(10),
    	cus_id int,
   	FOREIGN KEY(room_no) REFERENCES ROOMS(room_no) ON DELETE CASCADE,
     	FOREIGN KEY(ID) REFERENCES EMPLOYEE(ID) ON DELETE CASCADE,
 	FOREIGN KEY(cus_id) REFERENCES CUSTOMER(cus_id) ON DELETE CASCADE,
	PRIMARY KEY(room_no,ID,cus_id)   
);







