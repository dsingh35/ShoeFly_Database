INSERT INTO EMPLOYEE VALUES ('M100025', 'Kim', 'Administration', '456 49 Ave Vancouver','789456123');
INSERT INTO EMPLOYEE VALUES ('M100026', 'Mike', 'HR', '4486 45 Ave Vancouver','789486123');
INSERT INTO EMPLOYEE VALUES ('M100027', 'Emily', 'Services', '1561 done Ave Vancouver','745786123');
INSERT INTO EMPLOYEE VALUES ('S10025', 'Kely', 'Cleaning', '4889 79 Ave Surrey','789451563');
INSERT INTO EMPLOYEE VALUES ('S10026', 'Mow', 'Host', '165 49 Ave Vancouver','789456123');
INSERT INTO EMPLOYEE VALUES ('S100027', 'Merry', 'Chef', '79896 484 Ave Vancouver','459456123');


INSERT INTO MANAGEMENT VALUES ('M100025', '2500', 'Junior Admin');
INSERT INTO MANAGEMENT VALUES ('M100026', '3000', 'HR Supervisor');
INSERT INTO MANAGEMENT VALUES ('M100027', '2000', 'Junior Service');

INSERT INTO STAFF VALUES ('S10025', '15', '1 South', 'Full Time');
INSERT INTO STAFF VALUES ('S10026', '20', '2 South', 'Part Time');
INSERT INTO STAFF VALUES ('S10027', '25', 'Breakfast', 'Full Time');

INSERT INTO Accounts VALUES ('2017', '12', '10000','500000','300000','M100025');
INSERT INTO Accounts VALUES ('2017', '11', '9000','550000','240000','M100025');
INSERT INTO Accounts VALUES ('2017', '10', '11000','520000','350000','M100025');
INSERT INTO Accounts VALUES ('2017', '9', '12000','600000','450000','M100026');
INSERT INTO Accounts VALUES ('2017', '8', '10000','400000','150000','M100025');


INSERT INTO customer VALUES ('70001', 'lie', 'lie@langara.ca','500 45ave surrey');
INSERT INTO customer VALUES ('70002', 'Yu', 'yu@langara.ca','546 44ave surrey');
INSERT INTO customer VALUES ('70003', 'Mike', 'mike@langara.ca','658 78ave surrey');
INSERT INTO customer VALUES ('70004', 'Miller', 'miller@langara.ca','658 78ave Vancouver');
INSERT INTO customer VALUES ('70005', 'Jordan', 'Jordan@langara.ca','165 12ave surrey');

INSERT INTO serves VALUES ('70001', 'S10026');
INSERT INTO serves VALUES ('70002', 'S10026');
INSERT INTO serves VALUES ('70003', 'M100026');
INSERT INTO serves VALUES ('70004', 'M100025');
INSERT INTO serves VALUES ('70005', 'M100027');

INSERT INTO bill VALUES ('1000254','cash','1000','0','01/05/2017','70001');
INSERT INTO bill VALUES ('1000255','credit','500','10','18/06/2017','70002');
INSERT INTO bill VALUES ('1000256','debit','250','0','20/06/2017','70003');
INSERT INTO bill VALUES ('1000257','cash','750','50','22/06/2017','70004');
INSERT INTO bill VALUES ('1000258','cash','900','0','23/06/2017','70005');

INSERT INTO rooms VALUES ('100','Normal','150','true');
INSERT INTO rooms VALUES ('101','Special','450','false');
INSERT INTO rooms VALUES ('102','super','500','true');
INSERT INTO rooms VALUES ('103','Normal','150','false');
INSERT INTO rooms VALUES ('104','Normal','150','false');

INSERT INTO allots VALUES ('100','M100027','70001');
INSERT INTO allots VALUES ('101','M100025','70002');
INSERT INTO allots VALUES ('102','M100026','70003');
INSERT INTO allots VALUES ('104','M100025','70004');
INSERT INTO allots VALUES ('105','M100027','70005');



